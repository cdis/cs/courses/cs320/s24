# Lab 7: Flask

1. Install Flask by following directions in group part of P4.

2. Start working on the group part of P4 and build the web pages "index.html", "browse.html", and "donate.html".

# Screenshot Requirement

A screenshot that shows Flask is installed successfully.